package br.com.gabriel.javabasico.tiposprimitivos;

public class TiposPrimitivos {

    public static void apresentar(String propriedade, Object dado) {

        System.out.println(propriedade + " = " + dado);
    }
    public static void main(String[] args) {

        int idade = 21;
        boolean possuiCnh = true;
        double peso = 87.90;
        float altura = 1.83f;
        char sexo = 'M';
        apresentar("Idade", idade);
        apresentar("Possui cnh?", possuiCnh);
        apresentar("Peso", peso);
        apresentar("Altura", altura);
        apresentar("Sexo", sexo);
    }
}
