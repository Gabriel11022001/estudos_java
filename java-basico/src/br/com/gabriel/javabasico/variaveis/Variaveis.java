package br.com.gabriel.javabasico.variaveis;

public class Variaveis {

    public static void main(String[] args) {

        /**
         * Uma variável é um espaço de memória associada a um nome
         * que pode armazenar dados.
         */
        String nome = "Gabriel"; // string
        String sobrenome = "Rodrigues";
        String nomeCompleto = nome + " " + sobrenome;
        System.out.println("Nome: " + nome);
        System.out.println("Sobrenome: " + sobrenome);
        System.out.println("Nome completo: " + nomeCompleto);
        Integer idade = 21; // inteiro
        Double peso = 87.76; // double
        Boolean possuiCnh = true; // boolean
        System.out.println("Idade: " + idade);
        System.out.println("Peso: " + peso);
        System.out.println("Possui cnh=" + possuiCnh);

        if (possuiCnh) {
            System.out.println(nomeCompleto +  " possui habilitação para dirigir!");
        } else {
            System.out.println(nomeCompleto + " não possui habilitação para dirigir!");
        }

        int primeiroValor, segundoValor, soma; // variáveis do tipo inteiro
        primeiroValor = 12;
        segundoValor = 32;
        soma = primeiroValor + segundoValor;
        System.out.println("A soma entre " + primeiroValor + " e " + segundoValor + " é igual a " + soma);
    }
}
