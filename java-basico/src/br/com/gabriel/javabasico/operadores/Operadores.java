package br.com.gabriel.javabasico.operadores;

public class Operadores {

    public static void main(String[] args) {

        double primeiroValor = 12.0;
        double segundoValor = 2.0;
        // soma
        Double soma = primeiroValor + segundoValor;
        System.out.println(soma);
        // subtração
        Double subtracao = primeiroValor - segundoValor;
        System.out.println(subtracao);
        // multiplicação
        Double multiplicacao = primeiroValor * segundoValor;
        System.out.println(multiplicacao);
        // divisão
        Double divisao = primeiroValor / segundoValor;
        System.out.println(divisao);
        // resto da divisão
        Double restoDivisao = primeiroValor % segundoValor;
        System.out.println(restoDivisao);
        Integer valor = 1;
        valor = valor + 1;
        System.out.println(valor);
        valor += 1;
        System.out.println(valor);
        valor++;
        System.out.println(valor);
        valor = valor - 1;
        System.out.println(valor);
        valor -= 1;
        System.out.println(valor);
        valor--;
        System.out.println(valor);
        // operadores de comparação
        primeiroValor = 10.0;
        segundoValor = 10.0;
        System.out.println(primeiroValor == segundoValor); // igual a
        System.out.println(primeiroValor > segundoValor); // maior que
        System.out.println(primeiroValor < segundoValor); // menor que
        System.out.println(primeiroValor >= segundoValor); // maior ou igual a
        System.out.println(primeiroValor <= segundoValor); // menor ou igual a
        System.out.println(primeiroValor != segundoValor); // diferente de
        // operadores lógicos
        System.out.println("Gabriel".equals("Gabriel") && 1 > 2); // true e false = false
        System.out.println("Gabriel".equals("Gabriel") && 1 == 1); // true e true = true
        System.out.println("Gabriel".equals("Pedro") && 1 == 2); // false e false = false
        System.out.println("Gabriel".equals("Gabriel") || 1 > 2); // true ou false = true
        System.out.println("Gabriel".equals("Gabriel") || 1 == 1); // true ou true = true
        System.out.println("Gabriel".equals("Pedro") || 1 != 1); // false ou false = false
    }
}
